const puppeteer = require('puppeteer')
const config = require('./config.js');
const { IncomingWebhook } = require('@slack/client');
const webhook = new IncomingWebhook(config.SLACK.WEBHOOKURL.PROD);

try {
  (async () => {
    const browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
    })
    const page = await browser.newPage()
    await page.goto("https://qiita.com/", { waitUntil: 'networkidle2' })
    // await page.screenshot({path: 'qiita_screenshot.png'})

    //Organizationランキング 週間を取得
    const weekly = await page.evaluate(() => {
      const nameList = Array.from(document.querySelectorAll("div.ra-Organization_name"))
      const countList = Array.from(document.querySelectorAll("div.ra-Organization_contrib"))
      var datas = [];
      for (let i = 0; i < nameList.length; i++) {
        var data = {
          rank: i + 1,
          name: nameList[i].textContent,
          count: countList[i].textContent.replace('Posts', '')
        };
        datas.push(data);
      }
      return datas;
    });
    // console.log(weekly)

    //Organizationランキング タブを取得
    // const tab = await page.$$eval('div.ra-OrganizationList_tab > div:nth-child(2)', divs => {
    //   return divs.map(data => data.textContent)
    // });
    // console.log(tab)

    //Organizationランキング 月間をクリック
    await page.click("div.ra-OrganizationList_tab > div:nth-child(2)")
    await page.waitForSelector("div.ra-OrganizationList_content")

    //Organizationランキング 月間を取得
    const monthly = await page.evaluate(() => {
      const nameList = Array.from(document.querySelectorAll("div.ra-Organization_name"))
      const countList = Array.from(document.querySelectorAll("div.ra-Organization_contrib"))
      var datas = [];
      for (let i = 0; i < nameList.length; i++) {
        var data = {
          rank: i + 1,
          name: nameList[i].textContent,
          count: countList[i].textContent.replace('Posts', '')
        };
        datas.push(data);
      }
      return datas;
    });

    // console.log(monthly)
    //Slackに投稿
    postToSlack(formatToString(weekly), formatToString(monthly))
    await browser.close()
  })()
} catch (error) {
  console.error(error)
}

/**
 * Slackに週間/月間をwebhookで投稿する
 * @param {String} weekly 週間
 * @param {String} monthly 月間
 */
function postToSlack(weekly, monthly) {
  // console.log(data); return
  const date = getNowYMD()
  webhook.send({
    text: `*Qiita Organizationランキング ${date}*\n順位、投稿数、Organization名`,
    username: "bot",
    icon_emoji: ':slack:',
    attachments: [{
      color: '#5bde5f',
      fields: [
        { title: "週間", value: weekly },
      ],
    }, {
      color: '#5bc0de',
      fields: [
        { title: "月間", value: monthly }
      ],
      footer: "from https://qiita.com/",
      footer_icon:"https://cdn.qiita.com/assets/qiita-rectangle-71910ff07b744f263e4a2657e2ffd123.png",
    }],
  }, (err, res) => {
    if (err) {
      console.log('Error', err)
    }
    else {
      console.log('Message sent:', res)
    }
  })
}

/**
 * Slack投稿用に文字列に整形する
 * @param {Array} datas 週間 or 月間データ配列
 * 参考：https://api.slack.com/docs/message-attachments
 */
function formatToString(datas) {
  let str = ""
  const YUMEMI_NAME = config.YUMEMI_NAME
  datas.forEach(element => {
    // const rank = (" " + (element.rank)).slice(-2)
    const rank = config.EMOJI.RANK[element.rank]

    // const posts = (element.name == YUMEMI_NAME) ? `*${element.count}*` : element.count
    const posts = (" " + (element.count)).slice(-3)
    const name = (element.name == YUMEMI_NAME) ? `*${element.name}* ${config.EMOJI.YUMEMI}` : element.name
    
    str += `${rank} \`${posts}\` ${name}\n`
  });
  // datas.forEach(data => { str += `${data.name}, ${data.count}\n` })
  return str
}

function getNowYMD() {
  var dt = new Date();
  var y = dt.getFullYear();
  var m = ("00" + (dt.getMonth() + 1)).slice(-2);
  var d = ("00" + dt.getDate()).slice(-2);
  var result = y + "/" + m + "/" + d;
  return result;
}