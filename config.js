module.exports = {
  SLACK: {
    WEBHOOKURL: {
      DEV: "https://hooks.slack.com/services/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",
      PROD: "https://hooks.slack.com/services/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"
    },
  },
  EMOJI: {
    RANK: {
      1: ":first_place_medal:",
      2: ":second_place_medal:",
      3: ":third_place_medal:",
      4: ":four:",
      5: ":five:",
      6: ":six:",
      7: ":seven:",
      8: ":eight:",
      9: ":nine:",
      10: ":keycap_ten:",
    },
    YUMEMI: ":yumemi:"
  },
  YUMEMI_NAME: "株式会社ゆめみ"
}